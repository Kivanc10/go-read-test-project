package config

import (
	"errors"
	"fmt"
	"io/ioutil"
)

const fileHeader = "Sample Header"

var ErrnoConfigFile = errors.New("no confile file does exist")

type HeaderError struct {
	FaultyHeader string
}

func (e *HeaderError) Error() string {
	return fmt.Sprintf("Bad header , provided %s,expected : %s\n", e.FaultyHeader, fileHeader)
}

// load data from file
func Load() (string, error) {
	data, err := ioutil.ReadFile("/temp/config.txt")
	if err != nil { // if the file does not exist
		return "", ErrnoConfigFile
	}
	conf := string(data)
	fmt.Println("data : " + conf)
	if conf[0:8] != fileHeader {
		return "", &HeaderError{conf[0:8]}
	}
	return conf, nil

}
