package config

import (
	"fmt"
	"testing"
)

func TestLoad(t *testing.T) {
	if data, err := Load(); err != nil {
		fmt.Println("an error occured : ", err)
		t.Error()
	} else {
		fmt.Println("succeed : \t ", data)
	}
}
