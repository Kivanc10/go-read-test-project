package main

import (
	"fmt"

	"gitlab.com/Kivanc10/go-read-test-project/config"
)

func main() {
	if _, err := config.Load(); err != nil {
		fmt.Println(err)
	}
}
